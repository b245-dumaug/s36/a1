const Task = require('../Models/task.js');

// Controllers and functions

module.exports.getAll = (request, response) => {
  Task.find({})
    .then((result) => {
      return response.send(result);
    })
    .catch((error) => {
      return response.send(error);
    });
};

// Add task on our database
module.exports.createTask = (request, response) => {
  const input = request.body;

  Task.findOne({ name: input.name })
    .then((result) => {
      if (result !== null) {
        return response.send('The task is already existing');
      } else {
        let newTask = new Task({
          name: input.name,
        });

        newTask.save().then((save) => {
          return response.send('The task is successfully added!');
        });
      }
    })
    .catch((error) => {
      return response.send(error);
    })
    .catch((error) => {
      return response.send(error);
    });
};

// Controller that will delete the document that contains the given object.

module.exports.deleteTask = (request, response) => {
  let idToBeDeleted = request.params.id;

  //findByIdAndRemove - to find the document that contains the id and then delete the document
  Task.findByIdAndRemove(idToBeDeleted)
    .then((result) => {
      return response.send(result);
    })
    .catch((error) => {
      return response.send(error);
    });
};

// Controller for getting a specific task
module.exports.getTask = (request, response) => {
  let id = request.params.id;

  Task.findById(id)
    .then((result) => {
      return response.send(result);
    })
    .catch((error) => {
      return response.send(error);
    });
};

// Controller for changing the status of a task to complete
module.exports.completeTask = (request, response) => {
  let id = request.params.id;

  Task.findByIdAndUpdate(id, { status: 'complete' }, { new: true })
    .then((result) => {
      if (!result) {
        return response.send('Task not found.');
      }
      return response.send(result);
    })
    .catch((error) => {
      return response.send(error);
    });
};
