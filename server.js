const express = require('express');
const mongoose = require('mongoose');

const taskRoute = require('./Routes/taskRoute.js');

const app = express();

const port = 3001;

mongoose.connect(
  'mongodb+srv://admin:admin@batch245-dumaug.18ijatd.mongodb.net/s35-discussion?retryWrites=true&w=majority',
  {
    // Allows us to avoid any acurrent and future errors while connecting to mongodb.
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

// check connection
let db = mongoose.connection;

//error catcher
db.on('error', console.error.bind(console, 'Connection Error!'));

//Confirmation of the connection
db.once('open', () => console.log('We are now connected to the cloud!'));

// NOTE: Middlewares
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Routing
app.use('/tasks', taskRoute);

app.listen(port, () => console.log(`Sever is running at port ${port}`));

/*
  Separation of concersn:
    Model should be connected to the controller.
    Controller should be connected to the Routes.
    Route should be connected to the server/application
*/
