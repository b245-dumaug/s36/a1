const express = require('express');
const router = express.Router();

const taskController = require('../Controllers/taskControllers.js');

// Routes
// Route for getAll
router.get('/get', taskController.getAll);

//Route for createTask
router.post('/addTask', taskController.createTask);

//Route for deleteTask
router.delete('/deleteTasks/:id', taskController.deleteTask);

// Route for getting a specific task
router.get('/:id', taskController.getTask);

// Route for changing the status of a task to complete
router.put('/:id/complete', taskController.completeTask);

module.exports = router;
